{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <h2>{$language->translate('pet_visited')}</h2>

    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>{$language->translate('name')}</th>
            <th>{$language->translate('specie')}</th>
        </tr>
        </thead>
        <tbody id="tbodyList">
        {foreach $rows as $row}
            <tr>
                <td>{$row.Id}</td>
                <td>{$row.Nombre}</td>
                <td>{$row.Especie}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>

</div>
{include file="template/footer.tpl" title="footer"}
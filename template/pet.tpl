{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <h2>{$language->translate('pet_list')}</h2>

    <a href="http://localhost/examen2/pet/add">{$language->translate('new_pet')}</a>
    <br><br>
    <label>{$language->translate('filter')}</label><input type="text" id='filter'><br><br>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>{$language->translate('name')}</th>
            <th>{$language->translate('specie')}</th>
            <th>{$language->translate('datebirth')}</th>
            <th>{$language->translate('operations')}</th>

        </tr>
        </thead>
        <tbody id="tbodyList">
        {foreach $rows as $row}
            <tr id='row{$row.id}'>
                <td>{$row.id}</td>
                <td id="nombre{$row.id}">{$row.nombre}</td>
                <td id="especie{$row.id}">{$row.especie}</td>
                
                <td>{$row.fechaNacimiento}</td>
                <td>
                    <a href='#' class='deleteRecord'>{$language->translate('remove')}</a>
                    <a href="#" class="petVisit">{$language->translate('view')}</a>
                </td>
            </tr>
        {/foreach}
        </tbody>
    </table>

</div>
{include file="template/footer.tpl" title="footer"}
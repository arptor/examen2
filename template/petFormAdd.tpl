{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <br>
    <h2>{$language->translate('new_pet')}</h2>

    <form action="./insert/" method="POST">
        <label>{$language->translate('name')}</label><input type="text" name="name"><br>
        <label>{$language->translate('specie')}</label><input type="text" name="specie"><br>
        <label>{$language->translate('datebirth')}</label><input type="date" name="date"><br>
        <label></label><input type="submit" value="{$language->translate('send')}">
    </form>

</div>
{include file="template/footer.tpl" title="footer"}
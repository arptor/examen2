<!DOCTYPE html>

<html>
    <head>
        <title>{$language->translate('app_title')}</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{$url}public/css/default.css" />
        <script src="{$url}public/js/jquery.js" type="text/javascript"></script>
        {if isset($js) && count($js)}
        {foreach item=file from=$js}        
        <script src="{$url}public/js/{$file}" type="text/javascript"></script>        
        {/foreach}   
        {/if}
         
    </head>
    <body>
        <div id="header">
            <div id="title">
                {$language->translate('app_title')}
            </div>
            <div  style="float: left">
                <a href="{$url}{$lang}/index" >{$language->translate('index')}</a> 
                <a href="{$url}{$lang}/pet" >{$language->translate('pet')}</a>
                <a href="{$url}{$lang}/pet/report" >{$language->translate('pet_visited')}</a>
{*                <a href="{$url}{$lang}/user" >{$language->translate('user')}</a>
                <a href="{$url}{$lang}/product" >{$language->translate('product')}</a>
                <a href="{$url}{$lang}/order" >{$language->translate('orders')}</a>
*}            </div>

            <div  style="float: right ; padding-left:4em">
                {$user}
                <a href="{$url}{$lang}/login/{$login}" >{$login}</a>
            </div>
        </div>

            

<?php

require_once 'lib/Controller.php';

class Pet extends Controller
{

    function __construct()
    {
        parent::__construct('Pet');
    }

    public function index($pag)
    {
        if (isset($pag)) {
            $pag--;
            $rows = $this->model->getPag($pag);
        } else {
            $rows = $this->model->getAll();
        }

        $this->view->render($rows);
    }

    public function add()
    {
        $this->view->add();
    }

    public function report()
    {
        $this->view->petList($_SESSION['PetList']);
    }

    public function ajaxAddPetList()
    {
        $pet = $_POST;
        $_SESSION['PetList'][] = $pet;
        //echo 'Mascota ' . $pet['Nombre'];
    }

    public function insert()
    {
        $row = $_POST;
        $row['password'] = md5($row['password']);
        //var_dump($row);
        $this->model->insert($row);
        header('Location: ' . Config::URL . $_SESSION['lang'] . '/pet/');
    }

    public function ajaxGetDataFilter()
    {
        $text = $_POST['filter'];
        echo json_encode($this->model->filter($text));
    }

    public function ajaxDelete($id)
    {
        $this->model->delete($id);
        echo true;
    }

}

<?php

require_once 'lib/View.php';

class PetView extends View
{

    function __construct()
    {
        parent::__construct();
//        echo 'En la vista Index<br>';
    }

    public function render($rows, $template = 'pet.tpl')
    {
//        $this->smarty->assign('method', $this->getMethod());
//        $this->smarty->assign('js', $js);
        $js[] = 'ajaxPet.js';
        $this->smarty->assign('js', $js);

        $this->smarty->assign('rows', $rows);
        $this->smarty->display($template);
    }
    
    public function petList($rows, $template = 'petList.tpl')
    {
        $this->smarty->assign('rows', $rows);
        $this->smarty->display($template);
    }
    public function prueba()
    {
        $template = 'studyPrueba.tpl';
        $this->smarty->display($template);
    }

    public function add()
    {
        $template = 'petFormAdd.tpl';
        $this->smarty->assign('levelRows', $levelRows);
        $this->smarty->display($template);
    }

}

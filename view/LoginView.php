<?php
require_once 'lib/View.php';

class LoginView extends View{
    private $_message = "";
    function __construct()
    {
        parent::__construct();
//        $this->_message='xx';
    }
    
    public function render($plantilla='login.tpl')
    {
        $this->smarty->assign('message', $this->getMessage());
        $this->smarty->assign('url', Config::URL);
        $this->smarty->display($plantilla);
    }
    function getMessage()
    {
        return $this->_message;
    }

    function setMessage($message)
    {
        $this->_message = $message;
    }


}
$(document).ready(function () {
    var deleteRecord = function (id) {
        url = 'http://localhost/examen2/pet/ajaxDelete/' + id;
        $.get(url, function (data) {
//            alert(JSON.toLocaleString(data));
            if (data) {
                $('#row' + id).empty();
            } else {
                alert('Operación denegada');
            }
        }, 'json');
    };

    var filterTable = function (text) {
        url = 'http://localhost/examen2/pet/ajaxGetDataFilter/';
        $.post(url, {filter: text}, function (data) {
            //alert (data[0].nombre); return;
            var rows = data;

            $("#tbodyList").empty();
            for (var i = 0; i < rows.length; i++) {

                newRows = '<tr class="cell" id="row' + rows[i].id + '" idRow=' + rows[i].id + ' >';
                newRows = newRows + '<td > ' + rows[i].id + '</td>';
                newRows = newRows + '<td > ' + rows[i].nombre + '</td>';
                newRows = newRows + '<td > ' + rows[i].especie + '</td>';
                newRows = newRows + '<td > ' + rows[i].fechaNacimiento + '</td>';
                newRows = newRows + '<td > <a id="delete' + rows[i].id + '" class="deleteRecord">Borrar</a> </td></tr>';

                $("#tbodyList").append(newRows);
            }
        }, 'json');
    };


    $(document).on('click', ".deleteRecord", function (event) {
        event.preventDefault();
        idRow = $(this).parent().parent().children("td:first").text();
        deleteRecord(idRow);
    });


    $("#filter").keyup(function () {
        filterTable($(this).val());
    });


    $(".petVisit").click(function () {
        var id = $(this).parent().parent().attr('id');
        id = id.substring(3);
        var nombre = $('#nombre' + id).html();
        var especie = $('#especie' + id).html();
        url = 'http://localhost/examen2/pet/ajaxAddPetList/';
        $.post(url, {
            Id: id,
            Nombre: nombre,
            Especie: especie
        },
        function (data) {
            alert(data);
        },'html');

    });

});
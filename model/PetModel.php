<?php

require_once 'lib/Model.php';

class PetModel extends Model
{

    const PAG_SIZE = 10;

    function __construct()
    {
//        echo 'En el UserModel<br>';
        parent::__construct();
    }

    public function delete($id)
    {
        $this->_sql = "DELETE FROM mascota WHERE id=$id";
        $this->executeQuery();
    }

    public function filter($text)
    {
        $this->_sql = "SELECT id, nombre, especie, fechaNacimiento FROM mascota WHERE nombre LIKE '%$text%' or `especie` LIKE '%$text%'";
        $this->executeSelect();
        
        foreach ($this->_rows as &$current) {
            $date = date_create_from_format('Y-m-d', $current['fechaNacimiento']);
            $current['fechaNacimiento'] = date_format($date, 'd-m-Y');
        }
        return $this->_rows;
    }

    public function get($id)
    {
        $this->_sql = "SELECT id, nombre, especie, fechaNacimiento FROM mascota where id = $id ORDER BY id";
        $this->executeSelect();
        return $this->_rows[0];
    }

    public function getAll()
    {
        $this->_sql = "SELECT id, nombre, especie, fechaNacimiento FROM mascota";
        $this->executeSelect();
        foreach ($this->_rows as &$current) {
            $date = date_create_from_format('Y-m-d', $current['fechaNacimiento']);
            $current['fechaNacimiento'] = date_format($date, 'd-m-Y');
        }
        return $this->_rows;
    }

    public function countPages()
    {
        $this->_sql = "SELECT count(*) as count FROM producto";

        $this->executeSelect();
        $count = $this->_rows[0]['count'];
        return ceil($count / $this::PAG_SIZE);
    }

    public function getPag($pag)
    {
        $pag = $pag * $this::PAG_SIZE;
        $this->_sql = "SELECT id, nombre, especie, fechaNacimiento FROM mascota LIMIT " . $this::PAG_SIZE . " OFFSET $pag";

        $this->executeSelect();

        return $this->_rows;
    }

    public function insert($fila)
    {
        $this->_sql = "INSERT INTO mascota(nombre, especie, fechaNacimiento) "
                . "VALUES ('$fila[name]', '$fila[specie]', '$fila[date]')";

        $this->executeQuery();
    }

    public function update($row)
    {
        $this->_sql = "UPDATE estudio SET "
                . " codInterno='$row[codinterno]', "
                . " nombre='$row[nombre]',"
                . " idNivel=$row[idNivel],"
                . " codOficial='$row[codOficial]'"
                . " WHERE id = $row[id]";
        $this->executeQuery();
    }

}

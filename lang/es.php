<?php

$es = array(
    //Aplicacion
    'app_title' => 'Examen de marzo',
    'pet_list' => 'Lista de mascotas',
    'specie' => 'Especie',
    'datebirth' => 'Fecha de nacimiento',
    'remove' => 'Borrar',
    'view' => 'Visita',
    'filter' => 'Filtro',
    'new_pet' => 'Nueva mascota',
    'pet_visited' => 'Lista de visitas',
    //
    'operations' => 'Operaciones',
    'name' => 'Nombre',
    'user_list' => 'Lista de usuarios',
    'edit' => 'Editar',
    'delete' => 'Borrar',
    'new_user' => 'Nuevo usuario',
    'index' => 'Inicio',
    'help' => 'Ayuda',
    'user'=> 'Usuario',
    'error_password' => 'La contraseña debe tener entre 6 y 20 caracteres',
    //
    'study'=> 'Estudios',
    'study_list' => 'Lista de estudios',
    'new_study' => 'Nuevo estudio',
    'innerCode' => 'Codigo Interno',
    'officialCode' => 'Codigo Oficial',
    'level' => 'Nivel',
    //controles select
    'select_one' => 'seleccionar uno  ------------',
    
    
    //productos
    'product_list' => 'Lista de productos',
    'new_product' => 'Producto nuevo',
    

);
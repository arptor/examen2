-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-03-2016 a las 17:06:26
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `dweb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mascota`
--

CREATE TABLE IF NOT EXISTS `mascota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `especie` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `raza` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=117 ;

--
-- Volcado de datos para la tabla `mascota`
--

INSERT INTO `mascota` (`id`, `nombre`, `especie`, `raza`, `fechaNacimiento`) VALUES
(1, 'Tyssen', 'periquito', NULL, '2000-10-15'),
(2, 'Yalvé', 'periquito', NULL, '2006-09-24'),
(3, 'Tino', 'serpiente', NULL, '2001-07-28'),
(4, 'Iker', 'periquito', NULL, '2008-06-15'),
(5, 'Dingo', 'gato', NULL, '2001-03-21'),
(6, 'Gabone', 'gato', NULL, '2010-10-18'),
(7, 'Vincenzo', 'perro', NULL, '2012-06-28'),
(8, 'Tyrrell', 'tortuga', NULL, '2006-10-04'),
(9, 'Loras', 'perro', NULL, '2014-12-30'),
(10, 'Aureli', 'serpiente', NULL, '2004-03-25'),
(11, 'Alger', 'gato', NULL, '2002-04-02'),
(12, 'Jayson', 'gato', NULL, '2015-09-21'),
(13, 'Man', 'periquito', NULL, '2009-08-26'),
(14, 'Cromeé', 'perro', NULL, '2010-06-17'),
(15, 'Karim', 'perro', NULL, '2010-02-25'),
(16, 'Lamar', 'gato', NULL, '2007-01-23'),
(17, 'Noete', 'tortuga', NULL, '2007-07-19'),
(18, 'Krende', 'perro', NULL, '2001-11-18'),
(19, 'Noah', 'gato', NULL, '2012-10-18'),
(20, 'Bixo', 'gato', NULL, '2001-05-07'),
(21, 'Valan', 'perro', NULL, '2009-10-11'),
(22, 'Amán', 'perro', NULL, '2008-03-10'),
(23, 'Tahel', 'gato', NULL, '2008-05-17'),
(24, 'Rémi', 'loro', NULL, '2006-05-25'),
(25, 'Maxime', 'gato', NULL, '2012-11-30'),
(26, 'Vonaccio', 'gato', NULL, '2012-12-01'),
(27, 'Newman', 'tortuga', NULL, '2001-05-21'),
(28, 'Venite', 'serpiente', NULL, '2006-03-16'),
(29, 'Yoan', 'gato', NULL, '2011-03-04'),
(30, 'Sirio', 'canario', NULL, '2013-09-04'),
(31, 'Kurt', 'perro', NULL, '2010-05-27'),
(32, 'Bali', 'gato', NULL, '2013-06-04'),
(33, 'Heinek', 'serpiente', NULL, '2008-10-03'),
(34, 'Curro', 'perro', NULL, '2011-10-29'),
(35, 'Bilal', 'periquito', NULL, '2013-08-07'),
(36, 'Enzzo', 'loro', NULL, '2004-11-01'),
(37, 'Joale', 'perro', NULL, '2003-03-08'),
(38, 'Morgan', 'periquito', NULL, '2006-11-19'),
(39, 'Trigo', 'gato', NULL, '2014-10-02'),
(40, 'Amir', 'gato', NULL, '2000-05-14'),
(41, 'Vito', 'iguana', NULL, '2012-05-31'),
(42, 'Este', 'perro', NULL, '2006-11-23'),
(43, 'Galbi', 'perro', NULL, '2001-11-13'),
(44, 'Jalba', 'tortuga', NULL, '2014-06-22'),
(45, 'Brú', 'gato', NULL, '2003-07-04'),
(46, 'Neo', 'perro', NULL, '2010-09-20'),
(47, 'Crestín', 'tortuga', NULL, '2013-02-09'),
(48, 'Bonné', 'loro', NULL, '2009-04-25'),
(49, 'Pinto', 'perro', NULL, '2014-04-07'),
(50, 'Davant', 'tortuga', NULL, '2012-12-06'),
(51, 'Milo', 'tortuga', NULL, '2004-11-02'),
(52, 'Axic', 'gato', NULL, '2002-10-07'),
(53, 'Benne', 'perro', NULL, '2013-04-26'),
(54, 'Patch', 'gato', NULL, '2006-10-30'),
(55, 'Monty', 'serpiente', NULL, '2013-10-01'),
(56, 'Antón', 'serpiente', NULL, '2003-08-16'),
(57, 'Hali', 'loro', NULL, '2011-09-26'),
(58, 'Kassio', 'canario', NULL, '2015-11-17'),
(59, 'Franc', 'loro', NULL, '2013-01-21'),
(60, 'Pezzo', 'perro', NULL, '2005-01-23'),
(61, 'Gasper', 'perro', NULL, '2004-02-23'),
(62, 'Yoette', 'perro', NULL, '2010-10-24'),
(63, 'Béix', 'serpiente', NULL, '2005-04-17'),
(64, 'Dasel', 'iguana', NULL, '2005-07-06'),
(65, 'Thaysson', 'periquito', NULL, '2003-04-30'),
(66, 'Thin', 'gato', NULL, '2012-03-08'),
(67, 'Vinni', 'tortuga', NULL, '2001-12-02'),
(68, 'Idale', 'perro', NULL, '2005-11-26'),
(69, 'Anouk', 'hamster', NULL, '2006-12-05'),
(70, 'Benif', 'perro', NULL, '2012-01-26'),
(71, 'Frany', 'canario', NULL, '2008-10-23'),
(72, 'Volter', 'perro', NULL, '2007-07-16'),
(73, 'Cooper', 'loro', NULL, '2001-08-08'),
(74, 'Zick', 'tortuga', NULL, '2010-09-11'),
(75, 'Vivien', 'tortuga', NULL, '2003-04-22'),
(76, 'Zaimon', 'gato', NULL, '2013-03-15'),
(77, 'Dion', 'canario', NULL, '2003-06-14'),
(78, 'Trust', 'canario', NULL, '2008-01-21'),
(79, 'Indio', 'gato', NULL, '2008-11-11'),
(80, 'Chester', 'perro', NULL, '2007-06-19'),
(81, 'Yasuri', 'serpiente', NULL, '2004-03-08'),
(82, 'Kannuck', 'periquito', NULL, '2012-03-07'),
(83, 'Crunch', 'canario', NULL, '2008-09-03'),
(84, 'Hobbo', 'gato', NULL, '2008-04-06'),
(85, 'Bruc', 'gato', NULL, '2003-04-04'),
(86, 'Frezzio', 'perro', NULL, '2006-08-12'),
(87, 'Arcadi', 'gato', NULL, '2012-03-11'),
(88, 'Yanis', 'perro', NULL, '2004-02-21'),
(89, 'Crú', 'gato', NULL, '2003-05-22'),
(90, 'Yanet', 'gato', NULL, '2002-03-05'),
(91, 'Dents', 'perro', NULL, '2010-09-19'),
(92, 'Rossi', 'tortuga', NULL, '2008-06-17'),
(93, 'Gianni', 'perro', NULL, '2015-03-05'),
(94, 'Kurdo', 'perro', NULL, '2012-08-12'),
(95, 'Figo', 'perro', NULL, '2007-02-24'),
(96, 'Larry', 'gato', NULL, '2006-01-26'),
(97, 'Sukkar', 'iguana', NULL, '2001-11-09'),
(98, 'Ulisse', 'periquito', NULL, '2011-08-03'),
(99, 'Maco', 'perro', NULL, '2007-12-16'),
(100, 'Durán', 'canario', NULL, '2004-07-01'),
(101, 'Libio', 'periquito', NULL, '2001-03-14'),
(102, 'Kyle', 'periquito', NULL, '2003-09-01'),
(103, 'Volton', 'perro', NULL, '2000-12-15'),
(104, 'Bord', 'perro', NULL, '2005-12-21'),
(105, 'Auro', 'periquito', NULL, '2013-09-02'),
(106, 'Evan', 'perro', NULL, '2003-04-07'),
(107, 'Lorik', 'canario', NULL, '2008-03-18'),
(108, 'Nath', 'perro', NULL, '2006-03-12'),
(109, 'Lambert', 'serpiente', NULL, '2010-10-02'),
(115, 'Animalico', 'Perro', NULL, '2016-03-02');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
